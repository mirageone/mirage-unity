﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

using Mirage.Player;

namespace Mirage {
	
	public class MirageManager : MonoBehaviour {

		[SerializeField] private GameObject _playerPrefab;

		[Header("Player Spawn Area")]
		[SerializeField] private Transform _playerSpawnPosition;
		[SerializeField] private float _playerSpawnSquareWidth = 0;

		private ulong _currentUniverse = ulong.MinValue;

		private NetworkManager _networkManager;

		void Alive() {
			DontDestroyOnLoad( this );
		}

		// Use this for initialization
		void Start () {
			
			_networkManager = GetComponent<NetworkManager>();

			//var player = FindObjectOfType<Mirage.Player.Player>();
			//if ( player == null ) {
			//	Instantiate( _playerPrefab, _playerSpawn.position, _playerSpawn.rotation );
			//}

			_networkManager.dontDestroyOnLoad = true;
			_networkManager.runInBackground = true;

			_networkManager.playerPrefab = _playerPrefab;
			_networkManager.autoCreatePlayer = true;
			_networkManager.playerSpawnMethod = PlayerSpawnMethod.RoundRobin;

			this.CreateNetworkSpawnPositions();

			_networkManager.StartHost();
		}

		private void CreateNetworkSpawnPositions() {
			GameObject startPosition = new GameObject ("spawn1" );

			startPosition.transform.position = new Vector3( 
				_playerSpawnPosition.position.x + Random.Range(-_playerSpawnSquareWidth,_playerSpawnSquareWidth),
				_playerSpawnPosition.position.y,
				_playerSpawnPosition.position.z + Random.Range(-_playerSpawnSquareWidth,_playerSpawnSquareWidth) ); 

			startPosition.transform.rotation = _playerSpawnPosition.rotation;

			startPosition.AddComponent<NetworkStartPosition> ();
		}
	}
}