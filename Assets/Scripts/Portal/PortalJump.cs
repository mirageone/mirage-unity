﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Mirage.Portal {

	public class PortalJump : MonoBehaviour {

		[SerializeField] private string _toScenePath; // e.g. "Assets/scenes/simpa.unity"
		public string ToScenePath {
			get { return _toScenePath; }
			//set { _toSceneName = value; }
		}

		[SerializeField] private bool _disabled;
		public bool Disabled {
			get { return _disabled; }
			set { _disabled = value; }
		}

		// Use this for initialization
		void Start () {
			if ( _toScenePath == string.Empty ) {
				_disabled = true;
			}
			else if ( !_toScenePath.EndsWith( ".unity" ) ) {
				_toScenePath += ".unity";
			}
				
		}

		void OnTriggerEnter ( Collider passenger ) {
			
			this.SendThroughPortal( passenger.gameObject );
		}

		void SendThroughPortal( GameObject passenger ) {
			
			if (_disabled) {
				return;
			}

			Scene currentScene = SceneManager.GetActiveScene();

			if ( currentScene.path == _toScenePath ) {
				SendThroughPortalTeleport( passenger );
			}
			else {
				SendThroughPortalWormhome( passenger );
				// need new scene
			}
		}

		void SendThroughPortalTeleport( GameObject passenger ) {
			this._setSpawnPoint( passenger, _toScenePath );

		}

		void SendThroughPortalWormhome( GameObject passenger ) {
			
			StartCoroutine ( LoadYourAsyncScene( passenger ) );
		}

		IEnumerator LoadYourAsyncScene( GameObject passenger )
		{
			//Set the current Scene to be able to unload it later
			Scene fromScene = SceneManager.GetActiveScene();
			string fromScenePath = fromScene.path;

			// The Application loads the Scene in the background at the same time as the current Scene.
			AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(_toScenePath, LoadSceneMode.Additive);

			//Wait until the last operation fully loads to return anything
			while (!asyncLoad.isDone)
			{
				yield return null;
			}



			Scene toScene = SceneManager.GetSceneByPath( _toScenePath );

			//Move the GameObject (you attach this in the Inspector) to the newly loaded Scene
			SceneManager.SetActiveScene( toScene );
			
			//Unload the previous Scene
			/*AsyncOperation asyncUnload =*/ SceneManager.UnloadSceneAsync( fromScene );

			this._setSpawnPoint( passenger, fromScenePath );
			SceneManager.MoveGameObjectToScene( passenger, toScene );
			
			// Free up closed?
			// Resources.UnloadAllUnusedAssets() 

			//Wait until the last operation fully loads to return anything
			//while (!asyncUnload.isDone)
			//{
			//	yield return null;
			//}
		}

		private void _setSpawnPoint( GameObject passenger, string fromScenePath ) {
			
			Vector3 newSpawnPosition = this._getSpawnPoint( fromScenePath );

			if ( newSpawnPosition != Vector3.zero ) {
				passenger.transform.position = newSpawnPosition;
			}
			else {
				// Keep old coords?
				passenger.transform.position = Vector3.zero; // Zero more likely to exist...
			}
				
			// Co-ords for abydos portal x268, y0, z258
			//_passenger.transform.position = new Vector3 (268, 0, 258);
		}

		private Vector3 _getSpawnPoint( string fromScenePath ) {
			// Need to find spawn point in new scene.
			PortalSpawn[] spawns = FindObjectsOfType<PortalSpawn>();

			foreach ( PortalSpawn ps in spawns ) {

				if ( ps.gameObject.scene.path == _toScenePath ) {
					PortalJump pj = ps.gameObject.transform.parent.GetComponent<PortalJump>();

					if ( pj != this && pj.ToScenePath == fromScenePath ) {
						
						return ps.transform.position;
					}
				}
			}

			return Vector3.zero;
		}

		// Update is called once per frame
		// void Update () {
		//	
		//}
	}
}