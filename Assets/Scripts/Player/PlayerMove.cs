﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace Mirage.Player.FirstPerson {

    public class PlayerMove : FirstPersonController {

        // Use this for initialization
        //void Start () {
        //    
        //}
        //
        //void Update() {
        //
        //}

		// TODO: Move to c# Interface.
		private int _freeze;

		void Freeze () {
			_freeze += 1;

			if ( _freeze == 1 ) {
				// TODO. Stop movement. (For instance, while in portal transit)
			}
		}
		void Unfreeze() {
			_freeze -= 1;
			if ( _freeze == 0 ) {
				// TODO: Allow movement.
			}
		}
    }
}

