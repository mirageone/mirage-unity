﻿using UnityEngine;
using UnityEngine.Networking;

namespace Mirage.Player {
	
	public class Player : NetworkBehaviour
	{
		//private bool _alive = true;
		private Color _color = Color.red; // avatar ;)

		void Start() {
			
			if ( !isLocalPlayer ) {
				// Turn off unneeded parts for other players
				Camera c = this.GetComponentInChildren<Camera>();
				if ( c  ) {
					AudioListener al = c.GetComponent<AudioListener>();
					if ( al ) {
						GameObject.Destroy( al );
					}
					GameObject.Destroy( c );
				}
			}
		}

		public override void OnStartLocalPlayer()
		{
			MeshRenderer mr = GetComponent<MeshRenderer>();
			if( mr ) {
				mr.materials[mr.materials.Length-1].color = _color;
			}
		}
	}
}